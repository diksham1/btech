#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
//improved linear search
int arr[80000001] = {};

int main(int argc, char *argv[]) {
	assert(argc >= 3);
// ./a.out N key
	int N = atoi(argv[1]);
	int key = atoi(argv[2]);
		
	FILE *fptr = fopen("data.in", "r");
	int n = 0;

	while (n < N) {
		int val;
		fscanf(fptr, "%d", &val);	
		arr[n] = val;
		n++;
	}

	int i =0;

/*
	// Read Data set properly 	
	
	for(i = 0; i < n; i++) {
		printf("%d ", arr[i]);	
	}

*/
	arr[n] = key;
	fflush(stdin);
	clock_t start = clock();

	for (int i = 0;; i++) {
		if (arr[i] == key)	break;
	}	

	clock_t end = clock();

	double timeUsed = 1.0*(end-start)/CLOCKS_PER_SEC;

	printf("%d %lf\n",N, timeUsed);

	/* Use the time command in the shell: time -f $N%e -o <output_fileName> ./a.out */

	return 0;
}
