#include <stdio.h>
#include <stdlib.h>

int main() {
	const int N=700000000;
	FILE *fptr = fopen("unsortedData.in", "a");
	int i =0;
	while (i < N) {
		int val = random() % 2000;
		fprintf(fptr, "%d ", val);
		i++;
	}
	
	fprintf(fptr,"\n");
	fclose(fptr);

	return 0;
}
