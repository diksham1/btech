set terminal pngcairo size 1280,720
set output 'Sequential.png'
set title 'Sequential Search';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "timeTakenSeq.out" with linespoint linestyle 1;
