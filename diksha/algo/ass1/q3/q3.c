#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[80000001] = {};

int main(int argc, char *argv[]) {
	assert(argc >= 3);
// ./a.out N key
	int N = atoi(argv[1]);
	int key = atoi(argv[2]);
		
	FILE *fptr = fopen("data.in", "r");
	int n = 0;

	while (n < N) {
		int val;
		fscanf(fptr, "%d", &val);	
		arr[n] = val;
		n++;
	}
		
	int i =0;

/*
	// Read Data set properly 	
	
	for(i = 0; i < n; i++) {
		printf("%d ", arr[i]);	
	}

*/

	fflush(stdin);
	int fibM, fibM1, fibM2;
	fibM2 = 0;	fibM1 = 1;
	
	while (fibM < n) {
		fibM = fibM1+fibM2;
		fibM2 =fibM1;
		fibM1 = fibM;
	}

	int offset =0;
		clock_t start = clock();


	while (fibM > 1) {
		int mid = fibM2 + offset;
		if (mid >= n)	mid = n-1;
		if (arr[mid] == key) {
//			printf("Found : %d\n", mid);
			break;
		} else if (key < arr[mid]) {
			fibM = fibM2;
			fibM1 =fibM1-fibM2;
			fibM2 = fibM - fibM1;
		} else {
			fibM = fibM1;
			fibM1 = fibM2;
			fibM2 = fibM-fibM1;
		}
	}

	clock_t end = clock();

	double timeUsed = 1.0*(end-start)/CLOCKS_PER_SEC;

	printf("%d %lf\n",N, timeUsed);

	/* Use the time command in the shell: time -f $N%e -o <output_fileName> ./a.out */

	return 0;
}
