set terminal pngcairo size 1280,720
set output 'Both Binary and Sequential Search'
set title 'Linear and Binary Search;
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "timeTaken.out" with linespoint linestyle 1, "timeTakenSeq.out" with linespoint linestyle 2;
