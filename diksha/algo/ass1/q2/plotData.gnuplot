set terminal pngcairo size 1280,720
set output 'Fibonacci Search'
set title 'Fibonacci Search';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "timeTaken.out" with linespoint linestyle 1
