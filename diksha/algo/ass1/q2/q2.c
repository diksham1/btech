#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>

long long int arr[80000000] = {};

int main(int argc, char *argv[]) {
	assert(argc >= 3);

	int N = atoi(argv[1]);
	int key = atoi(argv[2]);	

//	cpu_set_t cpu;
	
//	CPU_ZERO(&cpu);
//	CPU_SET(0, &cpu);
//	pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpu);
	FILE *fptr = fopen("sortedData.in", "r");
	int n = 0;

	while (n < N) {
		long long int val;
		fscanf(fptr, "%lld", &val);	
		arr[n] = val;
		n++;
	}

	fclose(fptr);
	int i =0;

/*
	// Read Data set properly 	
	
	for(i = 0; i < n; i++) {
		printf("%d ", arr[i]);	
	}

*/

	int l = 0;
	int r = n-1;

	clock_t start = clock();
	
	while (l <= r) {
		int mid = l + (r-l)/2;
		if (arr[mid] == key)	break;
		if (key < arr[mid])	r = mid-1;
		else	l = mid+1;
	}	

	clock_t end = clock();

	double timeUsed = 1.0*(end-start)/CLOCKS_PER_SEC;

	printf("%d %lf\n",N, timeUsed);

	/* Use the time command in the shell: time -f $N%e -o <output_fileName> ./a.out */

	return 0;
}
