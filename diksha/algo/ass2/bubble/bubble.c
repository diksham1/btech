#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[100000000] = {};

void bubbleSort(int n) {
	clock_t start = clock();

	for (int i = 0; i < n; i++) {
		int swaps = 0;
		for (int j = 0; j < n-i-1; j++) {
			if (arr[j] > arr[j+1])	{
				int temp = arr[j];	
				arr[j] = arr[j+1];
				arr[j+1] = temp;
				swaps = 1;
			}
		}
		if (!swaps)	break;
	}

	clock_t end = clock();

	printf("%d %lf\n", n, 1.0*(end-start)/CLOCKS_PER_SEC);
}


int main(int argc, char *argv[]) {
	assert(argc >= 2);
	int n = atoi(argv[1]);
	FILE *fptr = fopen("unsortedData.in", "r");
	
	for (int i = 0; i < n; i++) {
		int val;	
		fscanf(fptr, "%d", &val);
		arr[i] = val;
	}

	bubbleSort(n);

	return 0;
}
