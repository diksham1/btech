set terminal pngcairo size 1280,720
set output 'BubbleSort Best'
set title 'BubbleSort Best Case Time';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "timeTaken.out" with linespoint linestyle 1
