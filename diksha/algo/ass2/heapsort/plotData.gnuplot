set terminal pngcairo size 1280,720
set output 'Heap Sort Average'
set title 'Heap Sort Average Case Time';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "timeTaken.out" with linespoint linestyle 1 linetype 1, x*log(x)*0.00000008 with linespoint linestyle 6 linetype 3, x*log(x)*0.00000007 with linespoint linestyle 6 linetype 4;
