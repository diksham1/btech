#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[100000000], heap[100000001];

void sort(int n) {
	clock_t start = clock();
		
	int k =1;
	for (int i = 0; i < n; i++) {
		heap[k] = arr[i];
		int p = k/2;
		int j = k;
		while (j > 0 && heap[j] < heap[p]) {
			int temp = heap[j];
			heap[j] = heap[p];	
			heap[p] = temp;
			p = p /2 ;
			j = j/2;			
		}
		k++;
	}

	for(int i =0 ; i < n; i++) { 
		arr[i] = heap[1];
		k--;
		heap[1] = heap[k];
		int i = 1;
		while (2*i < k) {
			int p = 2*(i);
			int j = 2*(i)+1;
			int option1 = heap[p], option2 = heap[p];
			if (j < k)	option2 = heap[j];
			if (option1 >= heap[i] && option2 >= heap[i])	break;
			if (option1 <= option2) {
				int temp = heap[i];
				heap[i] = heap[p];	
				heap[p] = temp;
				i = 2*i;
			} else {
				int temp = heap[j];	
				heap[j] = heap[i];
				heap[i] = temp;
				i = 2*i+1;
			}
		}
	}

	clock_t end = clock();
	printf("%d %lf\n", n, 1.0*(end-start)/CLOCKS_PER_SEC);
}

int main(int argc, char *argv[]) {
	assert(argc==2);
	int n = atoi(argv[1]);
	FILE *fptr = fopen("unsortedData.in", "r");	

	for (int i =0; i < n; i++) {	
		fscanf(fptr, "%d", &arr[i]);
	}
	sort(n);
}
