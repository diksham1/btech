#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[100000000], new[100000000];

void sort(int n) {
	int run = 2;
	clock_t start = clock();
	while (run <= n) {
		for (int i = 0; i < n; i+= run) {
			int n1 = i+run/2;
			int n2 = i+run;
			int l = i;	int r = n1, k = 0;
	
			while (l < n1 && r < n2) {
				if(arr[l] < arr[r]) {
					new[k++] = arr[l++];
				} else {
					new[k++] = arr[r++];	
				}
			}
			while (l < n1)	new[k++] = arr[l++];
			while (r < n2)	new[k++] = arr[r++];

			for (int p = 0; p < run; p++) {
				arr[i+p] = new[p];
			}
	
		}
		run = run * 2;
	}
	clock_t end = clock();
	printf("%d %lf\n", n, 1.0*(end-start)/CLOCKS_PER_SEC);
}

int main(int argc, char *argv[]) {
	assert(argc==2);
	int n = atoi(argv[1]);
	FILE *fptr = fopen("unsortedData.in", "r");	

	for (int i =0; i < n; i++) {	
		fscanf(fptr, "%d", &arr[i]);
	}
	sort(n);
/*	for(int i =0; i < n; i++)	printf("%d ", arr[i]);
	printf("\n");
*/	return 0;
}
