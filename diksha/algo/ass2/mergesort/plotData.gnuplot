set terminal pngcairo size 1280,720
set output 'Merge Sort Average'
set title 'Merge Sort Average Case Time';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "time2.out" with linespoint linestyle 1 linetype 1, x*log(x)*0.0000000150 with linespoint linestyle 6 linetype 3, x*log(x)*0.0000000100 with linespoint linestyle 5 linetype 4;
