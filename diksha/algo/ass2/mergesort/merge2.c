#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[100000000], new[100000000];

void mergesort(int l , int r) {
	if (l >= r)	return;
	int mid = l + (r-l)/2;
	int start = l;
	mergesort(l, mid);
	mergesort(mid+1,r);
	int k = 0;
	int p1 = l, p2 = mid+1;
	while ((p1 < mid+1) && (p2 <= r)) {
		if (arr[p1] < arr[p2]) {
			new[k++] = arr[p1++];
		}	else {
			new[k++] = arr[p2++];
		}
	}
	
	while (p1 < mid+1)	new[k++] = arr[p1++];
	while (p2 <= r)	new[k++] = arr[p2++];

	int i = 0;
	
	while (i < k)	arr[start++] = new[i++];
}

void sort(int n) {
	clock_t start = clock();
	mergesort(0, n-1);
	clock_t end = clock();
	printf("%d %lf\n", n, 1.0*(end-start)/CLOCKS_PER_SEC);
}

int main(int argc, char *argv[]) {
	assert(argc==2);
	int n = atoi(argv[1]);
	FILE *fptr = fopen("unsortedData.in", "r");	

	for (int i =0; i < n; i++) {	
		fscanf(fptr, "%d", &arr[i]);
	}

	sort(n);
	
	return 0;
}
