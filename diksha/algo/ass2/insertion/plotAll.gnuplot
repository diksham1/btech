set terminal pngcairo size 1280,720
set output 'all.png'
set title 'Best case and Average case runtimes together;
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"
plot "insertionBest.txt" using 1:(1000*($2)) with linespoint linestyle 1, "insertionAverage.txt" with linespoint linestyle 3;
