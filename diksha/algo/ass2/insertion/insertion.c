#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[100000000] = {};

void insertionSort(int n) {
	clock_t start = clock();

	for (int i = 1; i < n; i++) {
		int j = i;
		while ((j > 0) && arr[j] < arr[j-1])	{
			int temp = arr[j];	
			arr[j] = arr[j-1];
			arr[j-1] = temp;
			j--;
		}
	}

	clock_t end = clock();

	printf("%d %lf\n", n, 1.0*(end-start)/CLOCKS_PER_SEC);
}


int main(int argc, char *argv[]) {
	assert(argc >= 2);
	int n = atoi(argv[1]);
	FILE *fptr = fopen("sortedData.in", "r");
	
	for (int i = 0; i < n; i++) {
		int val;	
		fscanf(fptr, "%d", &val);
		arr[i] = val;
	}

	insertionSort(n);

	return 0;
}
