set terminal pngcairo size 1280,720
set output 'InsertionBest.png'
set title 'InsertionSort Best Case Time';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "timeTaken.out" with linespoint linestyle 1
