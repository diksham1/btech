#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	assert(argc == 2);
	const int N=atoi(argv[1]);
	FILE *fptr = fopen("oppSortedData.in", "w");
	int i =0;
	long long val = N;
	while (i < N) {
		fprintf(fptr, "%lld ", val);
		i++;
		val--;;
	}
	
	fprintf(fptr,"\n");
	fclose(fptr);

	return 0;
}
