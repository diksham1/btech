#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[100000000], new[100000000];

int getPivot(int l, int r) {
	return random() % (r-l+1) + l;
}
//  		777  	886 915 		1383
void quicksort(int l , int r) {
	if (l >= r)	return;
	int pivind = getPivot(l, r);
	int pivot = arr[pivind];
	int left = l-1;
	int i = l;
	int swap = 0;

	while (i <=r) {
		if (arr[i] < pivot) {		
			left++;
			int temp = arr[left];
			arr[left] = arr[i];
			arr[i] = temp;
			swap = 1;
		}
		i++;
	}

	if (!swap)	left++;

	quicksort(l, left);
	quicksort(left+1, r);
}

void sort(int n) {
	clock_t start = clock();
	quicksort(0, n-1);
	clock_t end = clock();
	printf("%d %lf\n", n, 1.0*(end-start)/CLOCKS_PER_SEC);
}

int main(int argc, char *argv[]) {
	assert(argc==2);
	int n = atoi(argv[1]);
	FILE *fptr = fopen("sortedData.in", "r");	

	for (int i =0; i < n; i++) {	
		fscanf(fptr, "%d", &arr[i]);
	}
	
	sort(n);

	return 0;
}
