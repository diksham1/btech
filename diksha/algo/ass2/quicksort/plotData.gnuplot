set terminal pngcairo size 1280,720
set output 'Quick Sort Average (Fixed Pivot)'
set title 'Quick Sort Average Case Time (Fixed Pivot)';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "fixed.txt" with linespoint linestyle 1 linetype 1, x*log(x)*0.0000000080 with linespoint linestyle 6 linetype 4, x*log(x)*0.000000012 with linespoint linestyle 6 linetype 2;
