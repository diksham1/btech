#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char *argv[]) {
	assert(argc == 2);
	const int N=atoi(argv[1]);
	FILE *fptr = fopen("sortedData.in", "w");
	int i =0;
	long long val = 1;
	while (i < N) {
		fprintf(fptr, "%lld ", val);
		i++;
		val++;;
	}
	
	fprintf(fptr,"\n");
	fclose(fptr);

	return 0;
}
