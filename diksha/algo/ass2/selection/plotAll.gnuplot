set terminal pngcairo size 1280,720
set output 'all.png'
set title 'Best case and Average case runtimes together'
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"
plot "selectionBest.txt" with linespoint linestyle 1, "selectionAverage.txt" with linespoint linestyle 3;
