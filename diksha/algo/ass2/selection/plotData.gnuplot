set terminal pngcairo size 1280,720
set output 'SelectionSortAverage.png'
set title 'SelectionSort Average Case Time';
set xlabel "Input Size (N)"
set ylabel "Time Taken (Seconds)"

plot "timeTaken.out" with linespoint linestyle 1
