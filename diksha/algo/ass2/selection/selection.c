#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

int arr[100000000] = {};

void selectionSort(int n) {
	clock_t start = clock();

	for (int i = 0; i < n; i++) {
		int swaps = 0;
		int minElement = i;
		for (int j = i; j < n; j++) {
			if (arr[j] > arr[minElement])	{
				minElement = j;
			}
		}
		int temp = arr[i];
		arr[i] = arr[minElement];
		arr[minElement] = temp;
	}

	clock_t end = clock();

	printf("%d %lf\n", n, 1.0*(end-start)/CLOCKS_PER_SEC);
}


int main(int argc, char *argv[]) {
	assert(argc >= 2);
	int n = atoi(argv[1]);
	FILE *fptr = fopen("unsortedData.in", "r");
	
	for (int i = 0; i < n; i++) {
		int val;	
		fscanf(fptr, "%d", &val);
		arr[i] = val;
	}

	selectionSort(n);

	return 0;
}
