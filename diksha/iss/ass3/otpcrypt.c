#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int s[3];

char *getxor(char *c, char *p)
{
    char *keybits = (char *)malloc(sizeof(char) * 100);
    for (int i = 0; i < strlen(p); i++)
    {
        if ((c[i] == '0' && p[i] == '1') || (c[i] == '1' && p[i] == '0'))
        {
            keybits[i] = '1';
        }
        else
        {
            keybits[i] = '0';
        }
    }
    printf("The xor is %s\n", keybits);
    return keybits;
}

void getseeds(char *keybits)
{
    for (int i = 0; i < 3; i++)
    {
        int num = 0;
        for (int j = i * 5; j < i * 5 + 5; j++)
        {
            num = num * 2 + (int)(keybits[j] - '0');
        }
        s[i] = num;
        printf("s[%d] is %d\n", i, num);
    }
}

int mulinv(int k)
{

    for (int i = 0; i < 26; i++)
    {
        if ((i * k) % 26 == 1)
            return i;
    }
    return -1;
}

int geta()
{
    int inv = mulinv(s[1] - s[0]);
    if (inv == -1)
    {
        printf("inverse of s1-s0 does not exist\n");
        exit(0);
    }
    return ((s[2] - s[1] + 26) * (inv)) % 26;
}

int getb(int a)
{
    return (s[1] - a * s[0] + 26) % 26;
}

int main()
{
    char *cipher = (char *)malloc(sizeof(char) * 100);
    char *plain = (char *)malloc(sizeof(char) * 100);

    printf("Enter the first 15 bits of ciphertext\n");
    scanf("%s", cipher);

    printf("Enter the first 15 bits of plaintext\n");
    scanf("%s", plain);

    char *keybits = getxor(cipher, plain);

    getseeds(keybits);

    int a = geta();
    int b = getb(a);

    printf("The value of s0, s1, s2 respectively is %d %d %d \n", s[0], s[1], s[2]);
    printf("A = %d, B = %d\n", a, b);
}