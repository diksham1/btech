#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int key[100];
int blocksize;

char *encrypt(char *plaintext)
{
    char *cipher = (char *)malloc(sizeof(char) * 100);
    int i = 0;
    int j = 0;
    int diff = blocksize - (strlen(plaintext) % blocksize);
    int k = strlen(plaintext);

    while (diff > 0)
    {
        plaintext[k] = 'x';
        k++;
        diff--;
    }
    //2 0 3 4 1
    plaintext[k] = '\0';

    for (; plaintext[i] != '\0'; i += blocksize)
    {
        for (int p = i; p < i + blocksize; p++)
        {
            cipher[j++] = (char)(plaintext[i + key[p % blocksize]] - 'a' + 'A');
        }
    }
    cipher[j] = '\0';
    return cipher;
}

char *decrypt(char *ciphertext)
{
    char *plaintext = (char *)malloc(sizeof(char) * 100);
    int i = 0;
    // int j = 0;

    for (; ciphertext[i] != '\0'; i += blocksize)
    {
        for (int p = i; p < i + blocksize; p++)
        {
            plaintext[i + key[p % blocksize]] = (char)(ciphertext[p] - 'A' + 'a');
        }
    }
    plaintext[i] = '\0';
    return plaintext;
}

// all capital letters
int main()
{
    char *c = (char *)malloc(sizeof(char) * 100);

    printf("Enter the blocksize\n");
    scanf("%d", &blocksize);

    printf("Enter the permutation key\n");

    for (int i = 0; i < blocksize; i++)
    {
        scanf("%d", &key[i]);
        key[i]--;
    }

    while (1)
    {
        printf("Press 1 for encryption and 2 for decryption\n");
        int choice;
        scanf("%d", &choice);
        if (choice == 1)
        {
            printf("Enter the plaintext\n");
            scanf("%s", c);

            char *cipher = encrypt(c);
            printf("The ciphertext is %s \n", cipher);
        }
        else
        {

            printf("Enter the ciphertext\n");
            scanf("%s", c);

            char *decryptedText = decrypt(c);

            printf("The decrypted text is %s\n", decryptedText);
        }
    }
    return 0;
}