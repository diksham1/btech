#include <stdio.h>
#include <stdlib.h>

int key[100];
int blocksize;

char *encrypt(char *plaintext)
{
    char *cipher = (char *)malloc(sizeof(char) * 100);
    int i = 0;
    int j = 0;

    for (; plaintext[i] != '\0'; i++)
    {
        int intval = (plaintext[i] - 'a');
        intval = (intval + key[j]) % 26;
        char cipherLetter = (char)('A' + intval);
        cipher[i] = cipherLetter;
        j = (j + 1) % blocksize;
    }
    cipher[i] = '\0';
    return cipher;
}

char *decrypt(char *ciphertext)
{
    char *plaintext = (char *)malloc(sizeof(char) * 100);
    int i = 0;
    int j = 0;

    for (; ciphertext[i] != '\0'; i++)
    {
        int intval = (ciphertext[i] - 'A');
        intval = (intval - key[j] + 26) % 26;
        char plaintextLetter = (char)('a' + intval);
        plaintext[i] = plaintextLetter;
        j = (j + 1) % blocksize;
    }
    plaintext[i] = '\0';
    return plaintext;
}

// all capital letters
int main()
{
    char *c = (char *)malloc(sizeof(char) * 100);

    printf("Enter the blocksize\n");
    scanf("%d", &blocksize);

    printf("Enter the keys\n");

    for (int i = 0; i < blocksize; i++)
    {
        scanf("%d", &key[i]);
    }

    while (1)
    {
        printf("Press 1 for encryption and 2 for decryption\n");
        int choice;
        scanf("%d", &choice);
        if (choice == 1)
        {
            printf("Enter the plaintext\n");
            scanf("%s", c);

            char *cipher = encrypt(c);
            printf("The ciphertext is %s \n", cipher);
        }
        else
        {

            printf("Enter the ciphertext\n");
            scanf("%s", c);

            char *decryptedText = decrypt(c);

            printf("The decrypted text is %s\n", decryptedText);
        }
    }
    return 0;
}