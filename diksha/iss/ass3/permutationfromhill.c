#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXSIZE 100
int KEYSIZE = 4;

int key[MAXSIZE][MAXSIZE] = {};
int keyInv[MAXSIZE][MAXSIZE] = {};

char *encryptcipher(char *plaintext)
{
	char *cipher = (char *)malloc(sizeof(char) * 200);
	int i = 0;
	int j = 0;
	int len = strlen(plaintext);
	int ptrow[KEYSIZE];

	for (i = 0; i < len; i += KEYSIZE)
	{
		int k = 0;
		for (k = 0; k < KEYSIZE && (i + k < len); k++)
		{
			ptrow[k] = (int)plaintext[i + k] - 'a';
		}
		while (k < KEYSIZE)
		{
			ptrow[k] = 25; //bogus character z
			k++;
		}

		for (int col = 0; col < KEYSIZE; col++)
		{
			int sum = 0;
			for (int row = 0; row < KEYSIZE; row++)
			{
				sum += key[row][col] * ptrow[row];
			}
			sum %= 26;
			cipher[j++] = (char)('A' + sum);
		}
	}

	cipher[j] = '\0';
	return cipher;
}

char *decryptcipher(char *ciphertext)
{
	char *plain = (char *)malloc(sizeof(char) * 100);
	int i = 0;
	int j = 0;
	int len = strlen(ciphertext);
	int ptrow[KEYSIZE];

	for (i = 0; i < len; i += KEYSIZE)
	{
		int k = 0;
		for (k = 0; k < KEYSIZE && (i + k < len); k++)
		{
			ptrow[k] = (int)ciphertext[i + k] - 'A';
		}
		while (k < KEYSIZE)
		{
			ptrow[k] = 25;
			k++;
		}

		for (int col = 0; col < KEYSIZE; col++)
		{
			int sum = 0;
			for (int row = 0; row < KEYSIZE; row++)
			{
				sum += keyInv[row][col] * ptrow[row];
			}
			sum %= 26;
			plain[j++] = (char)('a' + sum);
		}
	}

	plain[j] = '\0';
	return plain;
}

void cofactor(int key[][MAXSIZE], int cf[][MAXSIZE], int p, int q, int n)
{
	int i = 0, j = 0;
	for (int row = 0; row < n; row++)
	{
		for (int col = 0; col < n; col++)
		{
			if (row != p && col != q)
			{
				cf[i][j++] = key[row][col];
				if (j == n - 1)
				{
					j = 0;
					i++;
				}
			}
		}
	}
}

int determinant(int key[][MAXSIZE], int size)
{
	int d = 0;
	if (size == 1)
		return key[0][0];

	int cf[MAXSIZE][MAXSIZE];

	int val = 1;

	for (int col = 0; col < size; col++)
	{
		cofactor(key, cf, 0, col, size);
		d += val * key[0][col] * determinant(cf, size - 1);
		val *= -1;
	}

	if (d < 0)
	{
		d = 26 - ((-d) % 26);
	}

	d %= 26;

	return d;
}

int inverse(int x)
{
	for (int i = 0; i < 26; i++)
	{
		if (i * x % 26 == 1)
		{
			return i;
		}
	}
	return -1;
}

void adjoint(int key[][MAXSIZE], int adj[][MAXSIZE])
{
	if (KEYSIZE == 1)
	{
		adj[0][0] = 1;
		return;
	}

	int val = 1;
	int cf[MAXSIZE][MAXSIZE];

	for (int i = 0; i < KEYSIZE; i++)
	{
		for (int j = 0; j < KEYSIZE; j++)
		{
			cofactor(key, cf, i, j, KEYSIZE);
			if ((i + j) & 1)
			{
				val = 25;
			}
			else
			{
				val = 1;
			}
			adj[j][i] = ((val)*determinant(cf, KEYSIZE - 1)) % 26;
		}
	}
}

int matrixInv(int key[][MAXSIZE], int keyInv[][MAXSIZE])
{
	int det = determinant(key, KEYSIZE);
	if (det == 0 || inverse(det) == -1)
	{
		return 0;
	}

	int adj[MAXSIZE][MAXSIZE];
	adjoint(key, adj);

	for (int i = 0; i < KEYSIZE; i++)
		for (int j = 0; j < KEYSIZE; j++)
			keyInv[i][j] = (adj[i][j] * inverse(det) % 26);

	return 1;
}

// all capital letters
int main()
{
	char *c = (char *)malloc(sizeof(char) * 100);

	printf("Enter the block size\n");
	scanf("%d", &KEYSIZE);

	printf("Enter the key permutation\n");
	for (int i = 0; i < KEYSIZE; i++)
	{
		int val;
		scanf("%d", &val);
		val--;
		key[val][i] = 1;
	}

	if (!matrixInv(key, keyInv))
	{
		printf("Key Inverse does not exist\n");
	}

	while (1)
	{
		printf("Choose an option\n");
		printf("1. Encrypt with key\n");
		printf("2. Decrypt with key\n");
		int choice;
		scanf("%d", &choice);

		if (choice == 1)
		{
			printf("Enter the plaintext\n");
			scanf("%s", c);
			char *cipher = encryptcipher(c);
			printf("The ciphertext is %s \n", cipher);
		}
		else
		{
			printf("Enter the ciphertext\n");
			scanf("%s", c);
			char *decryptedText = decryptcipher(c);
			printf("The decrypted text is %s\n", decryptedText);
		}
	}
	return 0;
}
