#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//bit input
char *encrypt(char *plaintext, char *key)
{
    char *cipher = (char *)malloc(sizeof(char) * 100);
    int i = 0;

    for (; plaintext[i] != '\0'; i++)
    {
        int intval = (plaintext[i] - '0');
        intval = (intval ^ (key[i] - '0'));
        char cipherLetter = (char)('0' + intval);
        cipher[i] = cipherLetter;
    }
    cipher[i] = '\0';
    return cipher;
}

char *decrypt(char *ciphertext, char *key)
{
    char *plaintext = (char *)malloc(sizeof(char) * 100);
    int i = 0;

    for (; ciphertext[i] != '\0'; i++)
    {
        int intval = (ciphertext[i] - '0');
        intval = (intval ^ (key[i] - '0'));
        char plainLetter = (char)('0' + intval);
        plaintext[i] = plainLetter;
    }
    plaintext[i] = '\0';
    return plaintext;
}

char *generateOTP(int len)
{
    char *otp = (char *)malloc(sizeof(char) * 100);
    int j = 0;
    srand(time(0));

    while (len--)
    {
        otp[j] = (char)('0' + (rand() % 2));
        j++;
    }

    otp[j] = '\0';
    return otp;
}

// all capital letters
int main()
{
    char *c = (char *)malloc(sizeof(char) * 100);

    char *key;

    while (1)
    {
        printf("Press 1 for encryption and 2 for decryption\n");
        int choice;
        scanf("%d", &choice);
        if (choice == 1)
        {
            printf("Enter the plaintext\n");
            scanf("%s", c);

            key = generateOTP(strlen(c));
            printf("OTP = %s\n", key);

            char *cipher = encrypt(c, key);
            printf("The ciphertext is %s \n", cipher);
        }
        else
        {

            printf("Enter the ciphertext\n");
            scanf("%s", c);

            printf("Enter the OTP\n");
            scanf("%s", key);

            char *decryptedText = decrypt(c, key);

            printf("The decrypted text is %s\n", decryptedText);
        }
    }
    return 0;
}