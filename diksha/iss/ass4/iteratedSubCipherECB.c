#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

const int NUM_OF_ROUNDS = 4;
char *sboxarr[] = {
    "1110",
    "0100",
    "1101",
    "0001",
    "0010",
    "1111",
    "1011",
    "1000",
    "0011",
    "1010",
    "0110",
    "1100",
    "0101",
    "1001",
    "0000",
    "0111"};

char *sboxarrinv[] = {
    "1110", //0
    "0011", //1 
    "0100", //2
    "1000", //3
    "0001",//4
    "1100",//5
    "1010",//6
    "1111",//7
    "0111",//8
    "1101",//9
    "1001",//a
    "0110",//b
    "1011",//c
    "0010",// d
    "0000",// e
    "0101"};// f

int pboxarr[] = {1, 4, 5, 7, 3, 6, 2, 8};
int pboxarrinv[] = {1, 7, 5, 2, 3, 6, 4, 8};

char *getRoundKey(char *key, int roundnum)
{
    char *roundkey = (char *)malloc(sizeof(char) * 100);
    int start = 4 * roundnum - 3 - 1; // -1 for 0 based index
    int end = 4 * roundnum + 4 - 1;
    int j = 0;

    for (int i = start; i <= end; i++)
    {
        roundkey[j++] = key[i];
    }

    roundkey[j] = '\0';
    // printf("Start = %d, End = %d, roundkey = %s\n", start, end, roundkey);
    return roundkey;
}

char *whitenText(char *plaintext, char *keySchedule)
{
    char *whitenedplaintext = (char *)malloc(sizeof(char) * 100);

    for (int i = 0; i < 8; i++)
    {
        whitenedplaintext[i] = '0';
        if ((plaintext[i] == '0' && keySchedule[i] == '1') || (plaintext[i] == '1' && keySchedule[i] == '0'))
        {
            whitenedplaintext[i] = '1';
        }
    }

    whitenedplaintext[8] = '\0';
    // printf("Whitened text %s \n", whitenedplaintext);
    return whitenedplaintext;
}

char *applySBox(char *text, char *sboxarray[])
{
    char *result = (char *)malloc(sizeof(char) * 100);

    for (int i = 0; i < strlen(text); i += 4)
    {
        int num = 0;
        for (int j = i; j < i + 4; j++)
        {
            num = num * 2 + (int)(text[j] - '0');
        }
        char *val = sboxarray[num];
        for (int j = i; j < i + 4; j++)
        {
            result[j] = val[j - i];
        }
    }

    // printf("Sbox on %s -> %s\n", text, result);
    return result;
}

char *applyPBox(char *text, int pboxarray[])
{
    char *result = (char *)malloc(sizeof(char) * 100);
    for (int i = 0; i < strlen(text); i++)
    {
        result[i] = text[pboxarray[i] - 1];
    }
    // printf("Pbox on %s -> %s\n", text, result);
    return result;
}

//bit input
char *encrypt(char *plaintext, char *key, int roundnum, int numrounds)
{
    // printf("Round = %d\n", roundnum);

    char *keySchedule = (char *)malloc(sizeof(char) * 100);
    keySchedule = getRoundKey(key, roundnum);

    char *whitenedplaintext = (char *)malloc(sizeof(char) * 100);
    whitenedplaintext = whitenText(plaintext, keySchedule);

    char *substitutionResult = (char *)malloc(sizeof(char) * 100);
    substitutionResult = applySBox(whitenedplaintext, sboxarr);

    char *permutationResult = (char *)malloc(sizeof(char) * 100);
    if (roundnum == numrounds)
    {
        keySchedule = getRoundKey(key, roundnum + 1);
        whitenedplaintext = whitenText(substitutionResult, keySchedule);
        return whitenedplaintext;
    }
    else
    {
        permutationResult = applyPBox(substitutionResult, pboxarr);
        return encrypt(permutationResult, key, roundnum + 1, numrounds);
    }
}

char *decrypt(char *ciphertext, char *key, int roundnum, int numrounds)
{
    // printf("Round = %d\n", roundnum);

    if (roundnum == 1)
    {
        return whitenText(ciphertext, getRoundKey(key, 1));
    }

    char *keySchedule = (char *)malloc(sizeof(char) * 100);
    char *whitenedplaintext = (char *)malloc(sizeof(char) * 100);
    char *substitutionResult = (char *)malloc(sizeof(char) * 100);
    char *permutationResult = (char *)malloc(sizeof(char) * 100);

    if (roundnum == numrounds)
    {
        keySchedule = getRoundKey(key, roundnum + 1);
        ciphertext = whitenText(ciphertext, keySchedule);
        ciphertext = applySBox(ciphertext, sboxarrinv);
    }

    keySchedule = getRoundKey(key, roundnum);
    whitenedplaintext = whitenText(ciphertext, keySchedule);
    permutationResult = applyPBox(whitenedplaintext, pboxarrinv);
    substitutionResult = applySBox(permutationResult, sboxarrinv);

    return decrypt(substitutionResult, key, roundnum - 1, numrounds);
}

char *generateKey(int len)
{
    char *otp = (char *)malloc(sizeof(char) * 100);
    int j = 0;
    srand(time(0));

    while (len--)
    {
        otp[j] = (char)('0' + (rand() % 2));
        j++;
    }

    otp[j] = '\0';
    return otp;
}

// all capital letters
int main()
{
    char *c = (char *)malloc(sizeof(char) * 100);

    char *key;

    while (1)
    {
        printf("Press 1 for encryption and 2 for decryption\n");
        int choice;
        scanf("%d", &choice);
        if (choice == 1)
        {
            printf("Enter the plaintext\n");
            scanf("%s", c);

            key = generateKey(4 * NUM_OF_ROUNDS + 4 + 4);
            printf("Key = %s\n", key);

            char *cipher = encrypt(c, key, 1, NUM_OF_ROUNDS);
            printf("The ciphertext is %s \n", cipher);
        }
        else
        {

            printf("Enter the ciphertext\n");
            scanf("%s", c);

            printf("Enter the Key\n");
            scanf("%s", key);

            char *decryptedText = decrypt(c, key, NUM_OF_ROUNDS, NUM_OF_ROUNDS);

            printf("The decrypted text is %s\n", decryptedText);
        }
    }
    return 0;
}