#include <stdio.h>
#include <stdlib.h>

char * decryptcipher(char *ciphertext, int key1inv, int key2) {
	char *plaintext = (char *)malloc(sizeof(char)*100);
	int i = 0;
	
	for (; ciphertext[i] != '\0'; i++) {
		int intval = (ciphertext[i]-'A');
		intval = ((intval - key2 + 26) % 26 * key1inv + 26) % 26;
		char plaintextLetter = (char)('a' + intval);
		plaintext[i] = plaintextLetter;
	}
	plaintext[i] = '\0';
	return plaintext;
}

int keyInverse(int key) {
	for (int i =0; i < 26; i++) {
		if (i * key % 26 == 1) {
			return i;
		}
	}
	return -1;
}

// all capital letters
int main() {
	char *cipher = (char *)malloc(sizeof(char)*100);;
	int key1, key2;
	
	printf("Enter the ciphertext\n");
	scanf("%s", cipher);
	
	for (key1 = 0; key1 < 26; key1++) {
		int kinv = keyInverse(key1);
		if (kinv == -1) {
			continue;
		}

		for (key2 = 0; key2 < 26; key2++) {		
			char *decryptedText = decryptcipher(cipher, kinv, key2);
			printf("k1=%d, k2=%d, Decrypted text:  %s\n", key1, key2, decryptedText);
		}
	}
	return 0;
}
