#include <stdio.h>
#include <stdlib.h>

char * decryptcipher(char *ciphertext, int key) {
	char *plaintext = (char *)malloc(sizeof(char)*100);
	int i = 0;
	
	for (; ciphertext[i] != '\0'; i++) {
		int intval = (ciphertext[i]-'A');
		intval = (intval * key) % 26;
		char plaintextLetter = (char)('a' + intval);
		plaintext[i] = plaintextLetter;
	}
	plaintext[i] = '\0';
	return plaintext;
}

int keyInverse(int key) {
	for (int i =0; i < 26; i++) {
		if (i * key % 26 == 1) {
			return i;
		}
	}
	return -1;
}

// all capital letters
int main() {
	char *cipher = (char *)malloc(sizeof(char)*100);;
	int key;
	
	printf("Enter the ciphertext\n");
	scanf("%s", cipher);
	
	for (key = 0; key < 26; key++) {
		int kinv = keyInverse(key);
		if (kinv == -1) {
			continue;
		}
		char *decryptedText = decryptcipher(cipher, kinv);
		printf("Key: %d; Decrypted text: %s\n", key, decryptedText);
	}

	return 0;
}
