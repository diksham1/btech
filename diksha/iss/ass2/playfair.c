#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define KEYSIZE 5

int rowof[26];
int colof[26];
char key[KEYSIZE][KEYSIZE];
int XVAL = (int)('X' - 'A');
int JVAL = (int)('J' - 'A'); // j is omitted

char *encryptcipher(char *plaintext)
{
	char *cipher = (char *)malloc(sizeof(char) * 200);
	int len = strlen(plaintext);
	int j = 0;

	for (int i = 0; i < len;)
	{
		int a = (int)(plaintext[i] - 'a');
		int b = XVAL;
		int delta = 2;

		if (i + 1 < len)
			b = (int)(plaintext[i + 1] - 'a');

		i += 2;
		if (a == JVAL)
			a--; //convert to I
		if (b == JVAL)
			b--; //convert b to I if it was J

		if (a == b)
		{
			b = XVAL;
			i--;
		}
		printf("Encrypting %c and %c\n", (char)('A' + a), (char)('A' + b));

		// assumption a and b were not both 'X'
		if (rowof[a] == rowof[b])
		{
			cipher[j] = key[rowof[a]][(colof[a] + 1) % KEYSIZE];
			cipher[j + 1] = key[rowof[b]][(colof[b] + 1) % KEYSIZE];
		}
		else if (colof[a] == colof[b])
		{
			cipher[j] = key[(rowof[a] + 1) % KEYSIZE][colof[a]];
			cipher[j + 1] = key[(rowof[b] + 1) % KEYSIZE][colof[b]];
		}
		else
		{
			//row[a]col[b]
			//row[b]col[a]
			cipher[j] = key[rowof[a]][colof[b]];
			cipher[j + 1] = key[rowof[b]][colof[a]];
		}

		j += 2;
	}
	cipher[j] = '\0';
	return cipher;
}

char *decryptcipher(char *ciphertext)
{
	char *plain = (char *)malloc(sizeof(char) * 100);
	int len = strlen(ciphertext);
	int j = 0;

	for (int i = 0; i < len; i += 2)
	{
		int a = (int)(ciphertext[i] - 'A');
		int b = (int)(ciphertext[i + 1] - 'A');

		printf("Decrypting %c and %c\n", (char)('A' + a), (char)('A' + b));

		// assumption a and b were not both 'X'
		if (rowof[a] == rowof[b])
		{
			plain[j] = key[rowof[a]][(colof[a] - 1 + KEYSIZE) % KEYSIZE];
			plain[j + 1] = key[rowof[b]][(colof[b] - 1 + KEYSIZE) % KEYSIZE];
		}
		else if (colof[a] == colof[b])
		{
			plain[j] = key[(rowof[a] - 1 + KEYSIZE) % KEYSIZE][colof[a]];
			plain[j + 1] = key[(rowof[b] - 1 + KEYSIZE) % KEYSIZE][colof[b]];
		}
		else
		{
			plain[j] = key[rowof[a]][colof[b]];
			plain[j + 1] = key[rowof[b]][colof[a]];
		}

		plain[j] = (char)(plain[j] - 'A' + 'a');
		plain[j + 1] = (char)(plain[j + 1] - 'A' + 'a');

		j += 2;
	}

	plain[j] = '\0';
	return plain;
}

// all capital letters
int main()
{
	char *c = (char *)malloc(sizeof(char) * 100);
	;

	printf("Enter the key in matrix form\n");
	for (int i = 0; i < KEYSIZE; i++)
	{
		for (int j = 0; j < KEYSIZE;)
		{
			scanf("%c", &key[i][j]);
			if (key[i][j] == '\n' || key[i][j] == ' ')
				continue;
			int keyval = key[i][j] - 'A';
			rowof[keyval] = i;
			colof[keyval] = j;
			j++;
		}
	}

	while (1)
	{
		printf("Press 1 for encryption and 2 for decryption\n");
		int choice;
		scanf("%d", &choice);
		if (choice == 1)
		{
			printf("Enter the plaintext\n");
			scanf("%s", c);
			char *cipher = encryptcipher(c);
			printf("The ciphertext is %s \n", cipher);
		}
		else
		{

			printf("Enter the ciphertext\n");
			scanf("%s", c);
			char *decryptedText = decryptcipher(c);
			printf("The decrypted text is %s\n", decryptedText);
		}
	}
	return 0;
}
