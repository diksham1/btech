#include <stdio.h>
#include <stdlib.h>


char * decryptshiftcipher(char *ciphertext, int key) {
  char *plaintext = (char *)malloc(sizeof(char)*100);
  int i = 0;

  for (; ciphertext[i] != '\0'; i++) {
    int intval = (ciphertext[i]-'A');
    intval = (intval - key + 26) % 26;
    char plaintextLetter = (char)('a' + intval);
    plaintext[i] = plaintextLetter;
  }
  plaintext[i] = '\0';
  return plaintext;
}

int main() {
  char *cipher = (char *)malloc(sizeof(char)*100);;
  int key;

  printf("Enter the ciphertext\n");
  scanf("%s", cipher);

	for (int key = 0; key < 26; key++) {
  	char *decryptedText = decryptshiftcipher(cipher, key);
		 printf("Key = %d; Decrypted Text: %s\n", key, decryptedText);
	}

  return 0;
}

