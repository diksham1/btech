#include <stdio.h>
#include <stdlib.h>

char *encryptcipher(char *plaintext, int key)
{
	char *cipher = (char *)malloc(sizeof(char) * 100);
	int i = 0;

	for (; plaintext[i] != '\0'; i++)
	{
		int intval = (plaintext[i] - 'a');
		intval = (intval * key) % 26;
		char cipherLetter = (char)('A' + intval);
		cipher[i] = cipherLetter;
	}
	cipher[i] = '\0';
	return cipher;
}

char *decryptcipher(char *ciphertext, int key)
{
	char *plaintext = (char *)malloc(sizeof(char) * 100);
	int i = 0;

	for (; ciphertext[i] != '\0'; i++)
	{
		int intval = (ciphertext[i] - 'A');
		intval = (intval * key) % 26;
		char plaintextLetter = (char)('a' + intval);
		plaintext[i] = plaintextLetter;
	}
	plaintext[i] = '\0';
	return plaintext;
}

int keyInverse(int key)
{
	for (int i = 0; i < 26; i++)
	{
		if (i * key % 26 == 1)
		{
			return i;
		}
	}
	return -1;
}

// all capital letters
int main()
{
	char *c = (char *)malloc(sizeof(char) * 100);
	;
	int key;
	while (1)
	{
		printf("Press 1 for encryption and 2 for decryption\n");
		int choice;
		scanf("%d", &choice);
		if (choice == 1)
		{

			printf("Enter the plaintext\n");
			scanf("%s", c);

			printf("Enter the key\n");
			scanf("%d", &key);

			int kinv = keyInverse(key);
			if (kinv == -1)
			{
				printf("Key inverse does not exist, change key\n");
				return 0;
			}

			char *cipher = encryptcipher(c, key);
			printf("The ciphertext is %s \n", cipher);
		}
		else
		{
			printf("Enter the ciphertext\n");
			scanf("%s", c);

			printf("Enter the key\n");
			scanf("%d", &key);

			int kinv = keyInverse(key);
			if (kinv == -1)
			{
				printf("Key inverse does not exist, change key\n");
				return 0;
			}

			char *decryptedText = decryptcipher(c, kinv);

			printf("The decrypted text is %s\n", decryptedText);
		}
	}
	return 0;
}
