#include <stdio.h>
#include <stdlib.h>

char *encryptshiftcipher(char *plaintext, int key)
{
	char *cipher = (char *)malloc(sizeof(char) * 100);
	int i = 0;

	for (; plaintext[i] != '\0'; i++)
	{
		int intval = (plaintext[i] - 'a');
		intval = (intval + key) % 26;
		char cipherLetter = (char)('A' + intval);
		cipher[i] = cipherLetter;
	}
	cipher[i] = '\0';
	return cipher;
}

char *decryptshiftcipher(char *ciphertext, int key)
{
	char *plaintext = (char *)malloc(sizeof(char) * 100);
	int i = 0;

	for (; ciphertext[i] != '\0'; i++)
	{
		int intval = (ciphertext[i] - 'A');
		intval = (intval - key + 26) % 26;
		char plaintextLetter = (char)('a' + intval);
		plaintext[i] = plaintextLetter;
	}
	plaintext[i] = '\0';
	return plaintext;
}

// all capital letters
int main()
{
	char *c = (char *)malloc(sizeof(char) * 100);
	;
	int key;

	while (1)
	{
		printf("Press 1 for encryption and 2 for decryption\n");
		int choice;
		scanf("%d", &choice);
		if (choice == 1)
		{
			printf("Enter the plaintext\n");
			scanf("%s", c);

			printf("Enter the key\n");
			scanf("%d", &key);

			char *cipher = encryptshiftcipher(c, key);
			printf("The ciphertext is %s \n", cipher);
		}
		else
		{

			printf("Enter the ciphertext\n");
			scanf("%s", c);

			printf("Enter the key\n");
			scanf("%d", &key);

			char *decryptedText = decryptshiftcipher(c, key);

			printf("The decrypted text is %s\n", decryptedText);
		}
	}
	return 0;
}