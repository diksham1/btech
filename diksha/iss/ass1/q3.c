#include <stdio.h>

int main() {
	int mod = -1;
	printf("Enter the modulo\n");
	scanf("%d", &mod);

	int m = -1;
	printf("Enter the size of the array\n");
	scanf("%d", &m);
	
	int arr[3][3], inv[3][3];
	printf("Input the matrix\n");
	
	for (int i =0; i < m; i++) {
		for (int j = 0 ; j < m; j++) {
			scanf("%d", &arr[i][j]);
			inv[i][j] = arr[i][j];
		}
	}

	int det = -1;
	int detInv = -1;

	if (m == 1) {
		det = arr[0][0];
	} else if (m == 2) {
		det = arr[0][0]*arr[1][1] - arr[0][1]*arr[1][0];
	} else {
		det = arr[0][0] * (arr[1][1]*arr[2][2] % mod - arr[1][2]*arr[2][1] % mod + mod) % mod
				- arr[0][1] * (arr[1][0]*arr[2][2] % mod - arr[1][2]*arr[2][0] % mod + mod) % mod
				+ arr[0][2] * (arr[1][0]*arr[2][1] % mod - arr[2][2]*arr[2][0] % mod + mod) % mod;
	}

	det = (det + mod) % mod;
	for (int i = 0; i < mod; i++) {
		if (det*i%mod == 1) {
			detInv = i;
		}
	}
	if (detInv == -1) {
		printf("Inverse does not exist\n");
	} else if (m == 1) {
		printf("The inverse is \n");
		printf("%d\n", detInv);
	} else if (m == 2) {
		printf("The inverse is \n");
		inv[0][0] = arr[1][1] * detInv % mod;
		inv[0][1] = (arr[0][1] * detInv % mod * (-1) + mod) % mod;
		inv[1][0] = (arr[1][0] * detInv % mod * (-1) + mod) % mod;
		inv[1][1] = arr[1][1] * detInv % mod;
		for (int i =0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				printf("%d ", inv[i][j]);
			}
			printf("\n");
		}

	} else {
		printf("The inverse is \n");
		for (int i = 0; i < m; i++) {
			for (int j =0 ; j < m; j++) {
				int val = (- arr[(i+1)%3][(j+2)%3] 
									* arr[(i+2)%3][(j+1)%3] % mod
								+ arr[(i+2)%3][(j+2)%3] 
								* arr[(i+1)%3][(j+1)%3] % mod + mod) % mod; 
				val %= mod;
				inv[j][i] = val * detInv % mod;
			}
		}
		for (int i = 0; i < m; i++) {
			for (int j =0; j < m; j++) {
				printf("%d ", inv[i][j]);
			}
			printf("\n");
		}
	}

	return 0;
}
