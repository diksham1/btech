#include <stdio.h>
#include <stdlib.h>


int mygcd(int a, int b, int *x, int *y) {
	if (b == 0) {
		*x = 1;
		*y = 0;
		return a;
	}
	
	int *x1 = (int *)malloc(sizeof(int));
	int *y1 = (int *)malloc(sizeof(int));
	int d = mygcd(b, a%b, x1, y1);
	*x = *y1;
	*y = *x1 - *y1 * (a / b);
	return d;
}

int main() {
	int a, m, x,y;
	printf("Enter a and modulo\n");
	scanf("%d %d", &a, &m);
	int g = mygcd(a,m, &x, &y);
	if (g == 1) {
		printf("Modular multiplicative inverse is %d\n", (x+m)%m);
	}
}
