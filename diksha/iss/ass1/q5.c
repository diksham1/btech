#include <stdio.h>

int main() {
	printf("Enter the two numbers: ");
	int a, b;
	int r = -1, q = -1;
	scanf("%d %d", &a, &b);
	
	while(r != 0) {
		q = b / a;
		r = b % a;
		b = a;
		a = r;
	}

	printf("The GCD is %d \n", b);
	return 0;
}
