#include <stdio.h>

int power(int a, int b, int m) {
	if (b == 0)	return 1;
	int val = power(a, b/2, m) % m;
	val = val * val % m;
	if (b & 1) {
		val = val * a % m;
	}
	return val;
}

int calc(int sauce, int received, int p) {
	return sauce * received % p;
}

int main() {
	printf("Enter G, P\n");
	int g, p;
	scanf("%d %d", &g, &p);
	printf("Enter the private parameter: ");
	int pp, secret, received;
	scanf("%d", &pp);
	int sauce = power(g,pp, p);
	printf("Sending %d to other\n", sauce);

	printf("Enter the value received from other\n");
	scanf("%d", &received);

	secret = calc(sauce, received, p);

	printf("The secret key is %d\n", secret);
	return 0;
}
