#include <stdio.h>

int main() {
	int mod = -1;
	printf("Enter the modulo\n");
	scanf("%d", &mod);

	int num = -1;
	printf("Enter the number\n");
	scanf("%d", &num);
	num %= mod;


	int inverse = -1;
	int i = 0;

	for (i = 0; i < mod; i++) {
		if (num*i%mod == 1) {
			inverse = i;
		}
	}

	if (inverse == -1) {
		printf("The inverse does not exist\n");
	} else {
		printf("The inverse is %d\n", inverse);
	}

	return 0;
}
