#include <stdio.h>

int main() {
	int mod = -1;
	printf("Enter the modulo\n");
	scanf("%d", &mod);

	int num = -1;
	printf("Enter the number\n");
	scanf("%d", &num);
	num %= mod;

	// num + inv = 0;

	int inverse = (0-num+mod)%mod;
	printf("The inverse is %d\n", inverse);
	
	return 0;
}
