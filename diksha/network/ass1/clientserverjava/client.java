import java.io.*
import java.net.*

public class Client {
	public static void main(String args[]) {
		Socket s = new Socket("127.0.0.1", 9876);
		DataOutputStream dout = new DataOutputStream(s.getOutputStream());
		String msg = "SYN";
		dout.writeUTF(msg);
		s.close();
	}
}
