import java.io.*;
import java.net.*;

public class Server{
	public static void main(String args[]) {
		int port = 9876;
		ServerSocket ss = new ServerSocket(port);
		Socket s = ss.accept();
		while (true) {
			DataInputStream dataStream = new DataInputStream(s.getInputStream());
			String data = (String)dataStream.readUTF();
			System.out.println("The message is " + data);
			String reply = "ACK";
			DataOutputStream dout = new DataOutputStream(s.getOutputStream());	
			dout.writeUTF(reply);
		}
//		dout.close();
//		s.close();
	}
}
