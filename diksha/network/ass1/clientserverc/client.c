#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>

#define PORT 5057

int main() {
	int socketFD = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in clientAddr;

	bzero(&clientAddr, sizeof(clientAddr));


	clientAddr.sin_family = AF_INET;
	clientAddr.sin_port = htons(PORT);
	clientAddr.sin_addr.s_addr = inet_addr("127.0.0.1");	

	int ans  = connect(socketFD,(const struct sockaddr *)&clientAddr, sizeof(clientAddr));
	assert(ans != -1);
	char msg[50] = "";	

	while (1) {
		printf("Input: ");
		scanf("%s", msg);
		getchar();
		send(socketFD, (void *)&msg,50, 0);
		recv(socketFD, (void *)&msg,50, 0);
		printf("Received: %s \n", msg);
	}

	close(socketFD);
}
