#include <strings.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <netinet/in.h>
#include <netinet/ip.h>


#define PORT 8768

int main() {
	int socketFD = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in serverAddr, clientAddr;

	bzero(&serverAddr, sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);	

	assert(bind(socketFD, (struct sockaddr *)&serverAddr, 
			sizeof(serverAddr)) != -1);

	assert(listen(socketFD, 4) != -1);

	int lenServerData = sizeof(serverAddr);	

	int connectionID = accept(socketFD, (struct sockaddr *)&clientAddr, &lenServerData);
	
	assert(connectionID != -1);
	
	printf("Connected to client\n");
	
	char msg[50] = "Hello";	

	while(1) {
		recv(connectionID, (void *)&msg,50, 0);
		printf("Received: %s \n", msg);
		printf("Reply: ");
		scanf("%s", msg);
		send(connectionID, (void *)&msg, 50, 0);
	}

	close(socketFD);
}
