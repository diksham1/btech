#include<stdio.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<stdlib.h>
#include<netdb.h>
#include<netinet/in.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>

int main()
{
	struct sockaddr_in addrport,client;
	int sersock,serbi, serli,seracc;
	
	sersock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); 
 	
 	bzero(&addrport, sizeof(addrport));
	
	addrport.sin_family= AF_INET;
	addrport.sin_port = htons(8967);
	addrport.sin_addr.s_addr = htonl(INADDR_ANY);


	serbi=bind(sersock, (struct sockaddr *) &addrport, sizeof(addrport));
	if(serbi==-1)
	{ 	printf("binding failed ");
		if(errno==EADDRINUSE)
		printf("binding failed\n");
	}
	
	serli=listen(sersock,5);
	
	if(serli==-1)
	printf("listening failed\n");
	
	int size=sizeof(client);
	
	seracc= accept(sersock, (struct sockaddr *)&client,&size);
	if(seracc==-1)
	printf("acceptance socket failed\n");
	
	char message[20]="",msg[20]="";
	
	while(1)
	{
		read(seracc,message,20);
		printf("received %s",message);
		printf("send");
		scanf("%s",msg);
		send(seracc,msg,20,0);
	}
	close(sersock);
	
	//getchar();
	return(0);
} 
