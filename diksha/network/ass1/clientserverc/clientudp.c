#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>

#define PORT 5054

int main() {
	int socketFD = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in clientAddr;

	bzero(&clientAddr, sizeof(clientAddr));


	clientAddr.sin_family = AF_INET;
	clientAddr.sin_port = htons(PORT);
	clientAddr.sin_addr.s_addr = inet_addr("10.24.109.34");	

//	int ans  = connect(socketFD,(const struct sockaddr *)&clientAddr, sizeof(clientAddr));
//	assert(ans != -1);
	char msg[50] = "";	
	int clienLen = sizeof(clientAddr);

	while (1) {
		printf("Input: ");
		scanf("%s", msg);
		getchar();
		sendto(socketFD, (void *)&msg,50, 0, (const struct sockaddr *)&clientAddr,clienLen);
		recvfrom(socketFD, (void *)&msg,50, 0, (struct sockaddr *)&clientAddr, &clienLen);
		printf("Received: %s \n", msg);
	}

	close(socketFD);
}
