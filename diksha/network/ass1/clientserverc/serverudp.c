#include <strings.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <netinet/in.h>
#include <netinet/ip.h>


#define PORT 8768

int main() {
	int socketFD = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in serverAddr, clientAddr;

	bzero(&serverAddr, sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);	

	assert(bind(socketFD, (struct sockaddr *)&serverAddr, 
			sizeof(serverAddr)) != -1);

//	assert(listen(socketFD, 4) != -1);

	int lenServerData = sizeof(serverAddr);	

	//int connectionID = accept(socketFD, (struct sockaddr *)&clientAddr, &lenServerData);
	
//	assert(connectionID != -1);
	
//	printf("Connected to client\n");
	
	while(1) {	
		char msg[50] = "ACK";	
		recvfrom(socketFD, (void *)&msg,50, 0, (struct sockaddr *)&clientAddr, &lenServerData);
		printf("Received: %s \n", msg);
	//	printf("Reply: ");
	//	scanf("%s", msg);
		char reply[30] = "ACK";
		sendto(socketFD, (void *)&reply, 50, 0, (struct sockaddr *)&clientAddr, lenServerData);
	}

	close(socketFD);
}
