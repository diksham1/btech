import socket

host = '10.24.109.35'
port = 5081

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((host, port));

while True:
	print("Input: ", end=' ');
	msg = input();
	s.sendall(bytes(msg, 'utf-8'))
	reply = s.recv(1024)
	reply = reply.decode()
	print("Reply: ", reply);

s.close()
