import socket

host = '127.0.0.1'
port = 5088

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
	print("Input: ", end=' ');
	msg = input();
	s.sendto(bytes(msg, 'utf-8'), (host,port))
	reply, addr = s.recvfrom(1024)
	reply = reply.decode()
	print("Reply: ", reply);

s.close()
