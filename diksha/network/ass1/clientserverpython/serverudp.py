import socket

host = ''
port = 5088

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((host, port));

while True:
	data, addr = s.recvfrom(1024);
	data = data.decode()
	print("Received: ",data, " from ", addr)
	reply = "ACK"
	s.sendto(bytes(reply, 'utf-8'), addr)

sock.close()
s.close()
