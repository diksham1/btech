import socket

host = ''
port = 5656

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port));
s.listen(5)

sock, addr = s.accept()

print('Connected with: ', addr)

while True:
	data = sock.recv(4096);
	data = data.decode()
	print("Received: ",data)
	reply = "ACK"
	sock.sendall(bytes(reply, 'utf-8'))

sock.close()
s.close()
