#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <netinet/in.h>
#include <netinet/ip.h>


#define PORT 8759

int main() {
	int socketFD = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in serverAddr;

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = htons(INADDR_ANY);	

	assert(bind(socketFD, (struct sockaddr *)&serverAddr, 
			sizeof(serverAddr)) != -1);

	assert(listen(socketFD, 4) != -1);

	int lenServerData = sizeof(serverAddr);	

	int connectionID = accept(socketFD, 
		(struct sockaddr *)&serverAddr, &lenServerData);
	
	assert(connectionID != -1);
	
	printf("%d", connectionID);
	
	char msg[] = "Hello";	

	recv(connectionID, (void *)&msg,5, 0);
	
	printf("Received: %s \n", msg);
	
	close(socketFD);
}
