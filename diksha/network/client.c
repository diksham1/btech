#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <assert.h>
#include <unistd.h>

#define PORT 8759

int main() {
	int socketFD = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in clientAddr;

	clientAddr.sin_family = AF_INET;
	clientAddr.sin_port = htons(PORT);
	clientAddr.sin_addr.s_addr = htons(inet_addr("127.0.0.1"));	

	assert(connect(socketFD,(const struct sockaddr *)&clientAddr, sizeof(clientAddr)) != -1);
	
	char msg[] = "Hello";	

	send(socketFD, (void *)&msg,5, 0);
	
	close(socketFD);
}
