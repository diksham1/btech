#Server Code for the image sending server
#
#	Dependency: Ubuntu Machine with Shotwell installed


import socket
import os

host = ''
port = 5656

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
s.bind((host, port));

while True:
	data,addr = s.recvfrom(1024);
	print("connected with : ", addr);
	data = data.decode();	
	if (data == "SEND"):
		fhand = open("image.jpeg", "rb");
		reply = fhand.read();
		s.sendto(reply, addr);

s.close();
