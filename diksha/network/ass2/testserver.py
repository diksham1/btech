import http.server
import socketserver
from http.server import BaseHTTPRequestHandler


class HelloHTTPRequestHandler(BaseHTTPRequestHandler):
	message = "Acknowledged";	
	
	def do_GET(self):
		self.send_response(404);
		self.send_header('Content-type', 'text/html; charset=UTF-8')
		self.end_headers();
		self.wfile.write(self.message.encode('utf-8'));
		self.close_connection = True


if __name__ == '__main__':
	PORT = 8000
	Handler = http.server.SimpleHTTPRequestHandler
	httpd = socketserver.TCPServer(("", PORT), Handler)
	print("serving at port", PORT)
	httpd.serve_forever()
