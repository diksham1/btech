#Client program to send an image to the server

import socket
import os
import pickle

host = ''
port = 5656

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);


data = "SEND";
sock.sendto(bytes(data,'utf-8'), (host, port));
reply, addr = sock.recvfrom(20000000);
fhand = open("oldimage.jpeg", "wb");
fhand.write(reply);
#os.system("shotwell oldimage.jpeg");
fhand.close();

newimagehand = open('newimage.jpeg', 'wb');

for byte in reply:
	invertedByte = (255 ^ byte);
	newimagehand.write(int.to_bytes(invertedByte, 'little'));

newimagehand.close();

os.system("shotwell newimage.jpeg");

sock.close();
