#include <stdio.h>
#include <assert.h>

char first[27][27];
int numTerminals =-1, numNonTerminals = -1;
char startSymbol;
int numProductionRules[100];
char productionRules[100][100][100];
int terminals[256];
int nonTerminals[26];

/* directly storing terminals i.e no conversion as c-'a'. Instead directly c */

void computeFirst(int t) {
	int p =0;
	for (int i =0; i < numProductionRules[t]; i++) {
		for (int k =0; productionRules[t][i][k] != '\0'; k++) {
			char c = productionRules[t][i][k];
			if (c >= 'a' && c <= 'z') {
				first[t][p++] = c;	break;
			} 
			else {
				int f = (int)c-'A';
				if (f == t) {
					break;
				}
				computeFirst((int)c-'A');
				if (first[f][0] != '\0') {
					for (int l = 0; first[f][l] != '\0'; l++) {
						first[t][p++] = first[f][l];
					}
					break;
				}
			}
		}
	}
}	

int main() {
	/* productionRules[i][j][k] represents production Rule j of nonTerminal i; 
	And the kth character of that */	

	printf("Enter the number of terminals in the grammar\n");
	scanf("%d", &numTerminals);
	printf("Enter the number of non terminals in the grammar\n");
	scanf("%d", &numNonTerminals);
	printf("Enter the start Symbol (must be a capital letter)\n");
	getchar();
	scanf("%c", &startSymbol);
	getchar();
	assert(startSymbol >= 'A' && startSymbol <= 'Z');
	
/* Read terminals */
	int i = -1, j, k;	
	for (i = 0; i < numTerminals; i++) {
		printf("Enter terminal %d: ", i+1);
		char c;
		scanf("%c", &c);
		getchar();
		assert(!(c >= 'A' && c <= 'Z'));
		terminals[(int)c] = 1;
	}

/* Read non terminals */

	for (i = 0; i < numNonTerminals; i++) {
		printf("Enter non-termnial %d: ", i+1);
		char c;
		scanf("%c", &c);	getchar();
		assert(c >= 'A' && c <= 'Z');
		nonTerminals[c-'A'] = 1;
	}
	

/* Read production rules */
	for (i =0; i < 26; i++) {
		if (nonTerminals[i] == 0)	continue;
		char nonTerminal = (char)'A'+i;
		printf("Enter the	number of production rules for %c: ", nonTerminal);
		scanf("%d", &numProductionRules[i]);
		for (j =0; j < numProductionRules[i]; j++) {
			printf("%c -> ", nonTerminal);	
			scanf("%s", productionRules[i][j]); getchar();
			assert(productionRules[i][j] != "");
			for (k = 0; productionRules[i][j][k] != '\0'; k++) {
				char letter = productionRules[i][j][k];
				assert(nonTerminals[letter-'A'] || terminals[(int)letter]);
			}
		}
	}

//---------------------------------------------------------------------
	/* Printing the final grammar */

	for (i = 0; i < 26; i++) {
		if (nonTerminals[i] == 0)	continue;
		char nonTerminalLetter = (char)('A' + i);
		printf("%c -> ", nonTerminalLetter);
		for ( j = 0; j < numProductionRules[i]; j++)  {
			printf("/");
			for (k = 0; productionRules[i][j][k] != '\0'; k++) {
				printf("%c", productionRules[i][j][k]);
			}
		}
		printf("\n");
	}

	
//---------------------------------------------------------------------------

	printf("Printing the first: \n");

	for (int i=  0; i < 26; i++) {
		if (nonTerminals[i]) {
			computeFirst(i);
			printf("%c:", (int)('A'+i));
			for (int j = 0; first[i][j] != '\0'; j++) {
				printf("%c ", first[i][j]);
			}
			printf("\n");
		}
	}

//--------------------------------------------------------------------------
	return 0;
}
