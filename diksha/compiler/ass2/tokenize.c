#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

int transition[8][5] = {{4,4,4,4,4}, {2,6,5,1,4}, {2,2,4,3,4}, {1,1,1,1,1}, {4,4,4,3,4}, {4,7,4,4,4}, {4,6,5,3,4}, {4,7,4,3,4}};
int forwardPointer = 0, backwardPointer = 0;
char inputBuffer[128] = "";

/*
	0: alphabet or underscore
	1: a number (0-9)
	2: whitespace
	3: delimiter
*/

int getCharID(char c) {
	if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_'))	
		return 0;
	else if ((c >= '0') && (c <= '9'))
		return 1;
	else if (c == '.')	
		return 2;
	else if (c == ' ' || c == '\n' || c == '=' || c == '+' || c == '*' || c == '/' 
			|| c == '"' || c == '\'' || c == ';')
		return 3;
	else	return 4;
}

/* Fill the inputbuffer for double buffering */

bool fillBuffer(int forwardPointer, FILE *fptr) {
	int currentFillPosition = forwardPointer;
	int elementsToRead = 64;

	assert(fptr != NULL);	
	
	char c = fgetc(fptr);
	
	if (c == EOF)	return false;

	while(elementsToRead) {
		if (c == EOF) {	// if some buffer is not full, add a marker (space) and finish loading elements into it
			inputBuffer[currentFillPosition] = ' ';
			return true;
		}
		inputBuffer[currentFillPosition] = c;
		currentFillPosition++;
		elementsToRead--;
		c = fgetc(fptr);
	}
	
	return true;
}

void takeAction(int currentState, int previousState) {
	if (currentState == 3) {	// State 3 represents delimiter found i.e end of word

		if (previousState == 2) {
			printf("Identifier: "); 
		} else if (previousState == 6) {
			printf("Integer Number: ");
		} else if (previousState == 7) {
			printf("Floating Point Number: ");
		} else if (previousState == 4) {	// 4 represents I went in the lexical error state before and thus print lexical error
			printf("Lexical Error: ");
		}

	// perform two pointers 
		while (backwardPointer != forwardPointer) {
			if (getCharID(inputBuffer[backwardPointer]) != 3)
				printf("%c", inputBuffer[backwardPointer]);
			backwardPointer = (backwardPointer + 1) % 128;	//wrap around for pointers
		}

		printf("\n");
	}
}

int main(int argc, char *argv[]) {
	char *fileName = (char *)malloc(30*sizeof(char));

	//Read the fileName either from command line or input from user
	if (argc == 2) {
		fileName = argv[1];
	} else {
		fflush(stdin);
		scanf("%s", fileName);
	}

	printf("The fileName is %s\n", fileName);
	
	FILE *fptr = fopen(fileName, "r");

	assert(fptr != NULL);
	char c;
	int currentState = 1;
	int previousState = 0;

	// Process character by character and move through the automata

	while (1) {
		if ((forwardPointer == 0) || (forwardPointer == 64)) {
			if(!fillBuffer(forwardPointer, fptr))	break;
		}
		
		char c = inputBuffer[forwardPointer];
		int charID = getCharID(c);

		previousState = currentState;
		currentState = transition[currentState][charID];

		if (currentState == 3) {
			takeAction(currentState, previousState);
			currentState = 1;
		}

		forwardPointer = (forwardPointer + 1) % 128;
	}

	printf("Process finished\n");
	return 0;
}
