#include <stdio.h>

// 5 states q0, q1, q2, q3, q4

int transition[5][3] = {{1,2,4}, {1,2,4}, {3,4,4}, {3,4,4}, {4,4,4}};
int isFinal[5] = {0,1,0,1,0};

/* States are numbered from 0 to 4 (inclusive). Transition[i][j] represents the
	 transition taken by state i when given an input  with id j.
	 List of symbols are 0-9, '.', and all others.
	 0-9 is represented by input id of 0
	 '.' is represented by input id of 1
	 All other symbols lead to the dead state and are thus represented by 2
*/

int main() {
	char input[20];
	scanf("%s", input);
	
	int currentState = 0;
	int i = 0;
	
	while(input[i] != '\0') {
		if (input[i] == '.') {
			currentState = transition[currentState][1];
		} else if (input[i] >= '0' && input[i] <= '9') {
			currentState = transition[currentState][0];
		} else {
			currentState = transition[currentState][2];
		}
		i++;
	}

	if (isFinal[currentState]) {
		if (currentState == 1) {
			printf("It's an integer\n");
		} else if(currentState == 3) {
			printf("It's a floating point number\n");
		}
	} else {
		printf("String is not accepted\n");
	}

	return 0;
}
