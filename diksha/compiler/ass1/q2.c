#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

int transition[7][4] = {{1,4,4,4}, {2,5,6,5}, {2,4,6,2}, {0,2,6,6}, {0,0,0,0}, {4,4,4,4}, {6,6,3,6}};

int getCharID(char c) {
	if (c == '/')	return 0;
	else if (c == '\n')	return 1;
	else if (c == '*')	return 2;
	else return 3;
}

void takeAction(int currentState, char c) {
	if (currentState == 4) {
		FILE *fptr = fopen("duplicate.txt", "a");
		assert(fptr != NULL);
		fputc(c, fptr);
		fclose(fptr);
		return;
	} else if (currentState == 5) {
		takeAction(4, '/');
		takeAction(4, c);
	}
}

int main(int argc, char *argv[]) {
	char *fileName = (char *)malloc(30*sizeof(char));

	//Read the fileName either from command line or input from user
	if (argc == 2) {
		fileName = argv[1];
	} else {
		fflush(stdin);
		scanf("%s", fileName);
	}

	printf("The fileName is %s\n", fileName);
	
	FILE *fptr = fopen(fileName, "r");
	assert(fptr != NULL);
	char c;
	int currentState = 0;

	while ((c = fgetc(fptr)) != EOF) {
		int charID = getCharID(c);
		currentState = transition[currentState][charID];
		if (currentState == 4 || currentState == 5) {
			takeAction(currentState, c);
			currentState = 0;
		}
	}

	printf("Process finished\n");
	return 0;
}
