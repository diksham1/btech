/* Count vowels and consonants */

%{
	int vowel = 0; 
	int consonant = 0;
%}

%%
	
[AEIOUaeiou] {vowel++;}
[a-zA-Z] {consonant++;}

%%

main() {
	printf("Enter the string. Press Ctrl -D to terminate\n\n");
	yylex();
	printf("The number of vowels are %d \n", vowel);
	printf("The number of consonant are %d \n", consonant);
	return 0;
}
