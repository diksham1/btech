/* Q3 : count printf and scanf statements */

%{
	char readltr[] = "read";
	char writeltr[] = "write";
%}


%%
printf {fprintf(yyout, "%s", writeltr);}
scanf {fprintf(yyout, "%s", readltr);}
.	{fprintf(yyout, "%s", yytext);}
%%


main() {
	printf("Input Data: \n");
	yylex();
}
