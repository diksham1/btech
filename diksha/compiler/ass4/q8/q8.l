/* Q3 : count printf and scanf statements */

%{
	int isabb = 0, isabplus = 0;
%}


%%
"abb"[^( )]*"ccd\n" {isabb=1;}
.*"ab".* {isabplus=1;}
%%


main() {
	printf("Input Data: \n");
	yylex();
	if (isabb) {
		printf("abbccd type string \n");
	} else {
		printf("ab+ type string \n");
	}
	return 0;
}
