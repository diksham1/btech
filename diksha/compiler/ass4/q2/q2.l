/* Q2: Calculate number of positive & negative integers and fractions */

%{
	int posInt = 0, negInt = 0, posFrac = 0, negFrac = 0;	
%}


%%

\+?[1-9][0-9]*		{posInt++;}
-[1-9][0-9]* 			{negInt++;}
\+?[0-9]*\.[0-9]+	{posFrac++;}
-[0-9]*\.[0-9]+ 		{negFrac++;}
.	;
%%

main() {
	printf("Enter the numbers\n");	
	yylex();
	printf("Positive Integers: %d \n", posInt);
	printf("Negative Integers: %d \n", negInt);
	printf("Positive Fractions: %d \n", posFrac);
	printf("Negative Fractions: %d \n", negFrac);
	return 0;
}
