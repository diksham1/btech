/* Q3 : count printf and scanf statements */

%{
	int cchar=0, cword=0, cspace=0, cline = 0;
%}


%%
[ ] {cspace++;}
[\n] {cline++;}
[a-zA-Z0-9]+ {cword++;	cchar += yyleng;}
%%


main() {
	printf("Input Data: \n");
	yylex();
	printf("charCount: %d \n", cchar);
	printf("wordCount: %d \n", cword);
	printf("lineCount: %d \n", cline);
	printf("spaceCount: %d \n", cspace);
}
