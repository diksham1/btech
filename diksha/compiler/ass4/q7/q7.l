/* Q3 : count printf and scanf statements */

%{
	int singlecomment = 0, multicomment = 0;
%}


%%
\/\/.*\n {singlecomment++;}
\/\*[^(\*\/)]*\*\/ {multicomment++;}
%%


main() {
	yyin = fopen("input.txt", "r");
	yyout = fopen("output.txt", "w");
	yylex();
	printf("singlecomment: %d \n", singlecomment);
	printf("multicomment: %d \n", multicomment);
}
