%{
	#include <stdio.h>
	int cntiden = 0,	 cint = 0, cother = 0;
%}

%%
[_A-Za-z][0-9_A-Za-z]* cntiden++;
[\+]?|[-]?[0-9]+	cint++;
[a-zA-Z_0-9.]+	cother++;
%%

int main () {
	yylex();
	printf("Integers: %d \n", cint);
	printf("Identifier: %d \n", cntiden);
	printf("Others: %d \n", cother);
	return 0;
}
