/* Q3 : count printf and scanf statements */

%{
	int printfCount = 0, scanfCount = 0;
%}


%%
[p][r][i][n][t][f] {printfCount++;}
[s][c][a][n][f] {scanfCount++;}
%%


main() {
	printf("Input Data: \n");
	yylex();
	printf("printfCount: %d \n", printfCount);
	printf("scanfCount: %d \n", scanfCount);
}
