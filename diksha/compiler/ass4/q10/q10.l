%{
	#include <stdio.h>
	int flag = 0;
%}

%%
" and "|" AND " {flag=1;}
" or "|" OR "	{flag = 1;}
" but "|" BUT "	{flag = 1;}
	
%%

int main() {
	yylex();
	if (flag)	{
		printf("Compound Statement\n");
	} else {
		printf("Simple Statement\n");
	}
}
