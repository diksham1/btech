/* Q3 : count printf and scanf statements */

%{
	int intcount = 0, fraccount =0 ;
%}


%%
[\+]?0*[4][3-9]|[5-9][0-9]|[1-9][0-9][0-9]+ {intcount++;}
[\+]?0*[4][2]\.0*[1-9][0-9]*|[4][3-9]\.[0-9]+|[5-9][0-9]\.[0-9]+|[1-9][0-9][0-9]+\.[0-9]+ {fraccount++;}
[\+]?0*[0-9]|[0-3][0-9]|[4][0-2] {;}
[\+]?0*[0-9]\.[0-9]+|[0-3][0-9]\.0*[0-9]+|[4][0-1]\.[0-9]+|[4][2]\.0* {;}
%%


main() {
	printf("Input Data: \n");
	yylex();
	printf("intcount: %d \n", intcount);
	printf("fraccount: %d \n", fraccount);
}
